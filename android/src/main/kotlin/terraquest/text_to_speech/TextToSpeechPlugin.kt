package terraquest.text_to_speech

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.speech.tts.TextToSpeech
import java.util.*
import android.content.Context
import android.util.Log

/** TextToSpeechPlugin */
class TextToSpeechPlugin: FlutterPlugin, MethodCallHandler {
  private lateinit var channel : MethodChannel
  var mTTS: TextToSpeech? = null
  private lateinit var context: Context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "text_to_speech")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
  }

  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "text_to_speech")
      channel.setMethodCallHandler(TextToSpeechPlugin())
    }
  }


  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else if (call.method == "speak") {
      speak(call, result)
    }
    else if (call.method == "setSpeechRate") {
      setSpeechRate(call, result)
    }
    else if (call.method == "setPitch") {
      setPitch(call, result)
    }
    else if (call.method == "init") {
      init()
    } else {
      result.notImplemented()
    }
  }

  fun init() {
    mTTS = TextToSpeech(context, TextToSpeech.OnInitListener { status ->
        if (status != TextToSpeech.ERROR){
            mTTS?.setLanguage(Locale.US)
            mTTS?.setSpeechRate(0.9.toFloat())
        }
    })
  }

  fun setSpeechRate(call: MethodCall, result: Result) {
    val args = call.arguments as Map<String, Any?>
    val rate = args["rate"] as? Double ?: 1.0
    mTTS?.setSpeechRate(rate.toFloat())
  }

  fun setPitch(call: MethodCall, result: Result) {
    val args = call.arguments as Map<String, Any?>
    val pitch = args["pitch"] as? Double ?: 1.0
    mTTS?.setPitch(pitch.toFloat())
  }

  fun speak (call: MethodCall, result: Result) {
    val args = call.arguments as Map<String, Any?>
    val message = args["message"] as? String ?: ""
    mTTS?.speak(message, TextToSpeech.QUEUE_FLUSH, null)
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
