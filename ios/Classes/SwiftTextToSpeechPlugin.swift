import Flutter
import UIKit
import AVFoundation

public class SwiftTextToSpeechPlugin: NSObject, FlutterPlugin {
  let synthesizer: AVSpeechSynthesizer
  let methodChannel: FlutterMethodChannel
    var rate: Float = 0.5
    var pitch: Float = 1.0

  init (_ registrar: FlutterPluginRegistrar, methods: FlutterMethodChannel) {
    self.synthesizer = AVSpeechSynthesizer()
    self.methodChannel = methods
  }

  public static func register(with registrar: FlutterPluginRegistrar) {
    let methodChannel = FlutterMethodChannel(name: "text_to_speech", binaryMessenger: registrar.messenger())
    let instance = SwiftTextToSpeechPlugin(registrar, methods: methodChannel)
    registrar.addMethodCallDelegate(instance, channel: methodChannel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
            if (call.method == "speak") {
                speak(call, result: result)
            } else if (call.method == "setSpeechRate") {
                setSpeechRate(call, result: result)
            } else if (call.method == "setPitch") {
                setPitch(call, result: result)
            }else {
                result(FlutterMethodNotImplemented)
            }
  }

  private func speak(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            if let arguments = call.arguments {
                let args = arguments as! [String: Any]
                let message = args["message"] as? String ?? ""
                let utterance = AVSpeechUtterance(string: message)
                utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                utterance.rate = rate
                utterance.pitchMultiplier = pitch
                synthesizer.speak(utterance)
            }
       
        }
    }
    private func setSpeechRate(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
          do {
              if let arguments = call.arguments {
                  let args = arguments as! [String: Any]
                  let r = Float(args["rate"] as! Double)
                  print(r)
                  rate = r

              }
         
          }
      }
    private func setPitch(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
          do {
              if let arguments = call.arguments {
                  let args = arguments as! [String: Any]
                  let p = Float(args["pitch"] as! Double)
                  pitch = p
              }
         
          }
      }
}
