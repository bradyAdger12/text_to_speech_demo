# Text To Speech

 A demo flutter plugin that utilizes Android's Text To Speech module (https://developer.android.com/reference/android/speech/tts/TextToSpeech) 
 and iOS's speech synthesizer (https://developer.apple.com/documentation/avfoundation/avspeechsynthesizer).

# Getting Started
Clone repo into desired folder. Enter text and press 'Speak'


