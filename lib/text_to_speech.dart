import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextToSpeech {
  static const MethodChannel _channel = const MethodChannel('text_to_speech');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> speak({@required String message}) async {
    await _channel.invokeMethod('speak', {"message": message});
  }

  static Future<void> init() async {
    await _channel.invokeMethod('init');
  }

  static Future<void> setPitch({@required double pitch}) async {
    await _channel.invokeMethod('setPitch', {"pitch": pitch});
  }

  static Future<void> setSpeechRate({@required double rate}) async {
    await _channel.invokeMethod('setSpeechRate', {"rate": rate});
  }
}
