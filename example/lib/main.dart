import 'dart:io';

import 'package:flutter/material.dart';
import 'package:text_to_speech/text_to_speech.dart';
import 'package:flushbar/flushbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _textController = TextEditingController();
  double pitch = 1.0;
  double speechRate = 1.0;

  @override
  void initState() {
    TextToSpeech.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Text-To-Speech Demo'),
        ),
        body: Container(
            alignment: Alignment.center,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  setSpeechRateWidget(),
                  setPitchWidget(),
                  textBoxUI()
                ])));
  }

  Widget setSpeechRateWidget() {
    return Column(children: [
      Text('Speech Rate: ' + speechRate.toString()),
      Container(
          child: Slider(
        value: speechRate,
        divisions: 20,
        max: 2.0,
        min: 0,
        onChanged: (value) {
          setState(() {
            speechRate = value;
          }); 
          TextToSpeech.setSpeechRate(rate: Platform.isIOS ? speechRate / 2.0 : speechRate);
        },
      ))
    ]);
  }

  Widget setPitchWidget() {
    return Column(children: [
      Text('Pitch: ' + pitch.toString()),
      Container(
          child: Slider(
        value: pitch,
        divisions: 20,
        max: 2.0,
        min: 0,
        onChanged: (value) {
          setState(() {
            pitch = value;
          });
          TextToSpeech.setPitch(pitch: pitch);
        },
      ))
    ]);
  }

  Widget textBoxUI() {
    return Container(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
          constraints:
              BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: TextField(
              onSubmitted: (value) {
                TextToSpeech.speak(message: _textController.text);
              },
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.go,
              controller: _textController,
              decoration: InputDecoration(hintText: 'Enter Text...'))),
      FlatButton(
          onPressed: () {
            //plugin will go here
            if (_textController.text.length == 0) {
              Flushbar(
                duration: Duration(seconds: 2),
                backgroundColor: Colors.yellow,
                messageText: Text('Please enter text...',
                    style: TextStyle(color: Colors.black)),
              ).show(context);
            }
            TextToSpeech.speak(message: _textController.text);
          },
          child: Text('Speak'))
    ]));
  }
}
